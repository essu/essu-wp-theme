<?php


function esse_enqueue_styles() {

    $parent_style = 'generate-style-css';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'essu-style-css',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'esse_enqueue_styles' );

function essu_breadcrumbs() {
	if (function_exists('breadcrumb_trail')) {
		$args = array(
				'show_on_front'   => false,
				'show_browse'     => false,
			);
		breadcrumb_trail($args);
	}
}

function essu_the_attachments($post_id) {
	$this_post = pods( 'page', $post_id );
	$attachments = $this_post->field('_essu_attachment');

	if (!empty($attachments)) {

		echo '<ul class="cards">';

		foreach ($attachments as $attachment) {
			$id        = $attachment['ID'];
			$title     = $attachment['post_title'];
			$thumbnail = wp_get_attachment_image_src($id, 'medium');
			$url       = wp_get_attachment_url( $id );
			$image     = (empty($thumbnail)) ? '' : '<div class="image" style="background-image: url('.$thumbnail[0].');"></div>';
			$type      = $attachment['post_mime_type'];
			switch ($type) {
				case 'application/pdf':    $icon = 'file-pdf-o';   break;
				case 'application/msword': $icon = 'file-word-o';  break;
				case 'image/gif':          $icon = 'file-image-o'; break;
				case 'image/jpeg':         $icon = 'file-image-o'; break;
				case 'image/png':          $icon = 'file-image-o'; break;		
				default: $icon = 'file-text-o'; break;
			}

			printf('<li class="card"><a href="%s">
						<h4 class="title"><i class="fa fa-%s" aria-hidden="true"></i> %s</h4>
						<i class="fa fa-cloud-download" aria-hidden="true"></i>
						%s
					</a></li>', $url, $icon, $title, $image);


		}

		echo '</ul>';

	}
}


function essu_child_page_content($content) {
	$content = explode('<!--more-->', $content);
	$content = $content[0];
	return wpautop($content);
}


function essu_post_has_more($id) {
	$output = FALSE;
	$check_post = get_post($id);
	if ( strpos($check_post->post_content, '<!--more-->') ) {
		$output = TRUE;
	}
	return $output;
}


function essu_the_child_pages($pid) {

	if (!is_numeric($pid)) return;

	$args = array(
        'post_status' => 'publish',
        'post_type' => 'page',
        'post_parent' => $pid,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'nopaging' => true,
    );
	$pages = get_posts($args);
	if (empty($pages)) return;

	print '<ul class="child-pages">';

    foreach ($pages as $post) {
        setup_postdata($post);
        global $more; $more = 0;
		// HTML -> ?> 
	        <li>
	        	<?php edit_post_link('<i class="fa fa-pencil" aria-hidden="true"></i> Edit', '', '', $post->ID); ?>
	        	<div class="icon"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></div>
	        	<?php if ( essu_post_has_more($post->ID) ) { ?>
	        		<h3 class="title"><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></h3>
	        	<?php } else { ?>
	        		<h3 class="title"><?php echo $post->post_title; ?></h3>
	        	<?php } ?>
	        	<div class="content">
	        		<?php echo essu_child_page_content($post->post_content); ?>
	        		<?php if ( essu_post_has_more($post->ID) ) { ?>
	        			<p class="read-more-container"><a title="Level 1" class="read-more content-read-more" href="<?php the_permalink($post->ID); ?>">Read more</a></p>
	        		<?php } ?>
	        	</div>

				<?php
					$args = array(
						'child_of' => $post->ID,
						'echo' => false,
						'title_li' => false,
						'link_before' => '<i class="fa fa-arrow-circle-right" aria-hidden="true"></i> '
						);
					$children = wp_list_pages( $args );
					if( !empty($children) ) {
						print('<p style="margin-bottom: 0; font-weight: 400; font-size: 0.8em;">In this section:</p>');
						printf('<ul class="children">%s</ul>', $children);
					}
				?>

				<?php essu_the_attachments($post->ID); ?>
	        </li>
		<?php // <- end HTML
    }

    wp_reset_postdata();
    print '</ul>';

}

/**
 *  Child Pages widget
**/

// Register and load the widget
function essu_init_child_pages_widget() { register_widget( 'essu_child_pages_widget' ); }
add_action( 'widgets_init', 'essu_init_child_pages_widget' );

// Creating the widget 
class essu_child_pages_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

		// Base ID of your widget
		'essu_child_pages_widget', 

		// Widget name will appear in UI
		__('Child Pages', 'essu'), 

		// Widget description
		array( 'description' => __( 'Show child pages in flexible ways.', 'essu' ), ) 
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
		echo (!empty($args['before_widget'])) ? $args['before_widget'] : '';
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		$post_parents_unsafe = explode(',', $instance['parent_id']);
		$post_parents = array();
		foreach ($post_parents_unsafe as $post_parent) {
			$output = trim($post_parent);
			if ( is_numeric($post_parent)) {
				$post_parents[] = $post_parent;
			}
		}


		switch ($instance['order']) {
			case 'updated':
				$order_by = 'modified';
				$order = 'DESC';
				break;
			case 'published':
				$order_by = 'post_date';
				$order = 'DESC';
				break;
			case 'title':
				$order_by = 'title';
				$order = 'ASC';
				break;
			default:
				$order_by = 'menu_order';
				$order = 'ASC';
				break;
		}
		$limit = ( !empty($instance['n_posts']) && is_numeric($instance['n_posts']) ) ? $instance['n_posts'] : -1 ;

		$args = array(
			'post_status' => 'publish',
			'post_type' => 'page',
			'post_parent__in' => $post_parents,
			'orderby' => $order_by,
			'order' => $order,
			'posts_per_page' => $limit
	    );
		$pages = get_posts($args);
		if (empty($pages)) return;

		print '<ul class="child-pages-widget">';

	    foreach ($pages as $post) {
	        setup_postdata($post);
			global $more; $more = 0;
			// HTML -> ?> 
				<li>
					<div class="icon"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></div>
					<div class="title"><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></div>
				</li>
			<?php // <- end HTML
	    }

	    wp_reset_postdata();
	    print '</ul>';
		
		echo (!empty($args['after_widget'])) ? $args['after_widget'] : '';
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( '', 'essu' );
		}
		if ( isset( $instance[ 'parent_id' ] ) ) {
			$parent_id = $instance[ 'parent_id' ];
		} else {
			$parent_id = __( '', 'essu' );
		}
		if ( isset( $instance[ 'n_posts' ] ) ) {
			$n_posts = $instance[ 'n_posts' ];
		} else {
			$n_posts = __( '', 'essu' );
		}
		if ( isset( $instance[ 'order' ] ) ) {
			$order = $instance[ 'order' ];
		} else {
			$order = __( '', 'essu' );
		}
		// Widget admin form
		?>
			<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title: (optional)' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'parent_id' ); ?>"><?php _e( 'Parent ID:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'parent_id' ); ?>" name="<?php echo $this->get_field_name( 'parent_id' ); ?>" type="text" value="<?php echo esc_attr( $parent_id ); ?>" />
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'n_posts' ); ?>"><?php _e( 'Number of pages to show:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'n_posts' ); ?>" name="<?php echo $this->get_field_name( 'n_posts' ); ?>" type="number" value="<?php echo esc_attr( $n_posts ); ?>" />
			<small>Leave empty to show all child pages.</small>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" type="text" value="<?php echo esc_attr( $order ); ?>" />
			<small>Leave empty for normal page order. Or type 'published', 'updated', or 'title'.</small>
			</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['parent_id'] = ( ! empty( $new_instance['parent_id'] ) ) ? strip_tags( $new_instance['parent_id'] ) : '';
		$instance['n_posts'] = ( ! empty( $new_instance['n_posts'] ) ) ? strip_tags( $new_instance['n_posts'] ) : '';
		$instance['order'] = ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : '';
		return $instance;
	}
} // Class essu_child_pages_widget ends here



// Register and load the widget
function essu_init_filtered_content_widget() { register_widget( 'essu_filtered_content_widget' ); }
add_action( 'widgets_init', 'essu_init_filtered_content_widget' );

// Creating the widget 
class essu_filtered_content_widget extends WP_Widget {

	function __construct() {
		parent::__construct(

		// Base ID of your widget
		'essu_filtered_content_widget', 

		// Widget name will appear in UI
		__('Filtered Content List', 'essu'), 

		// Widget description
		array( 'description' => __( 'Show a list of content. Filter by post type and taxonomy.', 'essu' ), ) 
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		$title = ( !empty( $instance[ 'title' ] ) ) ? $instance[ 'title' ] : $title = '';
		$terms = ( !empty( $instance[ 'terms' ] ) ) ? $instance[ 'terms' ] : $terms = '';
		$types = ( !empty( $instance[ 'types' ] ) ) ? $instance[ 'types' ] : $types = '';
		$n_posts = ( !empty( $instance[ 'n_posts' ] ) ) ? $instance[ 'n_posts' ] : $n_posts = '';
		$order = ( !empty( $instance[ 'order' ] ) ) ? $instance[ 'order' ] : $order = '';

		$title = apply_filters( 'widget_title', $title );

		// before and after widget arguments are defined by themes
		echo (!empty($args['before_widget'])) ? $args['before_widget'] : '';
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		$terms_unsafe = explode(',', $terms);
		$terms = array();
		foreach ($terms_unsafe as $term) {
			$term = trim($term);
			$term = sanitize_title_with_dashes($term);
			$terms[] = $term;
		}

		if ( !empty($types) ) {
			$types_unsafe = explode(',', $types);
			$types = array();
			foreach ($types_unsafe as $type) {
				$type = trim($type);
				$type = sanitize_title_with_dashes($type);
				$types[] = $type;
			}
		} else {
			$types = array('post', 'page');
		}


		switch ($order) {
			case 'updated':
				$order_by = 'modified';
				$order = 'DESC';
				break;
			case 'published':
				$order_by = 'post_date';
				$order = 'DESC';
				break;
			case 'title':
				$order_by = 'title';
				$order = 'ASC';
				break;
			default:
				$order_by = 'menu_order';
				$order = 'ASC';
				break;
		}
		$limit = ( !empty($n_posts) && is_numeric($n_posts) ) ? $n_posts : 5 ;

		$filter_args = array(
			'post_status' => 'publish',
			'post_type' => $types,
			'orderby' => $order_by,
			'order' => $order,
			'posts_per_page' => $limit
	    );
		if (!empty($terms[0])) {
			$filter_args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'category',
						'field'    => 'slug',
						'terms'    => $terms
					),
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'slug',
						'terms'    => $terms
					),
				);
			
		}

		$pages = get_posts($filter_args);
		if (empty($pages)) return;

		print '<ul class="child-pages-widget">';

	    foreach ($pages as $post) {
	        setup_postdata($post);
			global $more; $more = 0;
			// HTML -> ?> 
				<li>
					<div class="icon"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?></div>
					<div class="title"><a href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a></div>
				</li>
			<?php // <- end HTML
	    }

	    wp_reset_postdata();
	    print '</ul>';
		
		echo (!empty($args['after_widget'])) ? $args['after_widget'] : '';
	}
			
	// Widget Backend 
	public function form( $instance ) {
		$title = ( isset( $instance[ 'title' ] ) ) ? $instance[ 'title' ] : $title = __( '', 'essu' ); ;
		$terms = ( isset( $instance[ 'terms' ] ) ) ? $instance[ 'terms' ] : $terms = __( '', 'essu' ); ;
		$types = ( isset( $instance[ 'types' ] ) ) ? $instance[ 'types' ] : $types = __( '', 'essu' ); ;
		$n_posts = ( isset( $instance[ 'n_posts' ] ) ) ? $instance[ 'n_posts' ] : $n_posts = __( '', 'essu' ); ;
		$order = ( isset( $instance[ 'order' ] ) ) ? $instance[ 'order' ] : $order = __( '', 'essu' ); ;
		
		
		// Widget admin form
		?>
			<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title: (optional)' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'terms' ); ?>"><?php _e( 'Terms:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'terms' ); ?>" name="<?php echo $this->get_field_name( 'terms' ); ?>" type="text" value="<?php echo esc_attr( $terms ); ?>" />
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'types' ); ?>"><?php _e( 'Types:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'types' ); ?>" name="<?php echo $this->get_field_name( 'types' ); ?>" type="text" value="<?php echo esc_attr( $types ); ?>" />
			<small>Example: 'page, post'</small>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'n_posts' ); ?>"><?php _e( 'Number of pages to show:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'n_posts' ); ?>" name="<?php echo $this->get_field_name( 'n_posts' ); ?>" type="number" value="<?php echo esc_attr( $n_posts ); ?>" />
			<small>Leave empty to show 5 posts.</small>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e( 'Order' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>" type="text" value="<?php echo esc_attr( $order ); ?>" />
			<small>Leave empty for normal page order. Or type 'updated' or 'title'.</small>
			</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['terms'] = ( ! empty( $new_instance['terms'] ) ) ? strip_tags( $new_instance['terms'] ) : '';
		$instance['types'] = ( ! empty( $new_instance['types'] ) ) ? strip_tags( $new_instance['types'] ) : '';
		$instance['n_posts'] = ( ! empty( $new_instance['n_posts'] ) ) ? strip_tags( $new_instance['n_posts'] ) : '';
		$instance['order'] = ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : '';
		return $instance;
	}
} // Class essu_filtered_content_widget ends here


class essu_page_taxonomy {

  function __construct(){

    add_action( 'init', array( $this, 'taxonomies_for_pages' ) );

    /**
     * Want to make sure that these query modifications don't
     * show on the admin side or we're going to get pages and
     * posts mixed in together when we click on a term
     * in the admin
     */
    if ( ! is_admin() ) {
      // add_action( 'pre_get_posts', array( $this, 'category_archives' ) );  // disabled
      // add_action( 'pre_get_posts', array( $this, 'tags_archives' ) ); // disabled
    }

  }

  /**
   * Registers the taxonomy terms for the post type
   */
  function taxonomies_for_pages() {
      register_taxonomy_for_object_type( 'post_tag', 'page' );
      register_taxonomy_for_object_type( 'category', 'page' );
  }

  /**
   * Includes the tags in archive pages
   *
   * Modifies the query object to include pages
   * as well as posts in the items to be returned
   * on archive pages
   */
  function tags_archives( $wp_query ) {

    if ( $wp_query->get( 'tag' ) )
      $wp_query->set( 'post_type', 'any' );

  }

  /**
   * Includes the categories in archive pages
   *
   * Modifies the query object to include pages
   * as well as posts in the items to be returned
   * on archive pages
   */
  function category_archives( $wp_query ) {

    if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
      $wp_query->set( 'post_type', 'any' );

  }

}
$essu_page_taxonomy = new essu_page_taxonomy();


/** ==============================
 *   GeneratePress overrides
 *  ==============================
 */

function essu_customize_register( $wp_customize ) {

	$wp_customize->remove_section( 'generatepress_upsell_section' );
	$wp_customize->remove_section( 'body_section' );
	$wp_customize->remove_section( 'font_section' );
	$wp_customize->remove_control( 'blog_get_addon_desc' );
	$wp_customize->remove_section( 'custom_css' );

}
add_action( 'customize_register', 'essu_customize_register', 20);

// Override post meta
if ( ! function_exists( 'generate_post_meta' ) )  {
	add_action( 'generate_after_entry_title', 'generate_post_meta' );
	function generate_post_meta() {
		return; // ignore lines below and product no output;
		if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta">
				<?php generate_posted_on(); ?>
			</div><!-- .entry-meta -->
		<?php endif;
	}
}

// Override Page featured image placement
add_action('generate_before_content','generate_featured_page_header_inside_single', 10);
function generate_featured_page_header_inside_single() {
	if ( function_exists( 'generate_page_header' ) ) return;
	if ( is_single() || is_page() ) {
		generate_featured_page_header_area('page-header-image-single');
	}
}
function generate_featured_page_header() {}

// Override taxonomy display
if ( ! function_exists( 'generate_entry_meta' ) ) {
	function generate_entry_meta() {
		echo '<div class="entry-meta">';
		?>
			<span class="date">
				Published on <?php the_date(); ?>.
				<?php if ( get_the_date() != get_the_modified_date() ) { ?>
				Last updated <?php the_modified_date(); ?>.
				<?php } ?>
			</span>
		<?php
		$categories = apply_filters( 'generate_show_categories', true );
		$tags = apply_filters( 'generate_show_tags', true );
		$comments = apply_filters( 'generate_show_comments', true );

		// $categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'generatepress' ) );
		// if ( $categories_list && $categories ) {
		// 	echo apply_filters( 'generate_category_list_output', sprintf( '<span class="cat-links"><span class="screen-reader-text">%1$s </// span>%2$s</span>',
		// 		_x( 'Categories', 'Used before category names.', 'generatepress' ),
		// 		$categories_list
		// 	) );
		// }

		$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'generatepress' ) );
		if ( $tags_list && $tags ) {
			echo apply_filters( 'generate_tag_list_output', sprintf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
				_x( 'Tags', 'Used before tag names.', 'generatepress' ),
				$tags_list
			) );
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) && $comments ) {
			echo '<span class="comments-link">';
				comments_popup_link( __( 'Leave a comment', 'generatepress' ), __( '1 Comment', 'generatepress' ), __( '% Comments', 'generatepress' ) );
			echo '</span>';
		}

		echo '</div>';
	}
}


// Override GeneratePress grid widths for main content + sidebars (values must be integers divisible by 5)
//add_filter( 'generate_right_sidebar_width', function() { return '20'; } );
//add_filter( 'generate_left_sidebar_width', function() { return '20'; } );


if ( ! function_exists( 'generate_construct_footer_widgets' ) ) :
/**
 * Build our footer widgets
 * @since 1.3.42
 */
add_action( 'generate_footer','generate_construct_footer_widgets', 5 );
function generate_construct_footer_widgets() {
	// Get how many widgets to show
	$widgets = generate_get_footer_widgets();
	
	if ( !empty( $widgets ) && 0 !== $widgets ) : 
	
		// Set up the widget width
		$widget_width = '';
		if ( $widgets == 1 ) $widget_width = '100';
		if ( $widgets == 2 ) $widget_width = '50';
		if ( $widgets == 3 ) $widget_width = '33';
		if ( $widgets == 4 ) $widget_width = '25';
		if ( $widgets == 5 ) $widget_width = '20';
		?>
		<div id="footer-widgets" class="site footer-widgets">
			<div <?php generate_inside_footer_class(); ?>>
				<div class="inside-footer-widgets">
					<?php if ( $widgets >= 1 ) : ?>
						<div class="footer-widget-1 grid-parent grid-<?php echo absint( apply_filters( 'generate_footer_widget_1_width', $widget_width ) ); ?> tablet-grid-<?php echo absint( apply_filters( 'generate_footer_widget_1_tablet_width', '50' ) ); ?> mobile-grid-100">
							<?php dynamic_sidebar('footer-1'); ?>
						</div>
					<?php endif;
					
					if ( $widgets >= 2 ) : ?>
					<div class="footer-widget-2 grid-parent grid-<?php echo absint( apply_filters( 'generate_footer_widget_2_width', $widget_width ) ); ?> tablet-grid-<?php echo absint( apply_filters( 'generate_footer_widget_2_tablet_width', '50' ) ); ?> mobile-grid-100">
						<?php dynamic_sidebar('footer-2'); ?>
					</div>
					<?php endif;
					
					if ( $widgets >= 3 ) : ?>
					<div class="footer-widget-3 grid-parent grid-<?php echo absint( apply_filters( 'generate_footer_widget_3_width', $widget_width ) ); ?> tablet-grid-<?php echo absint( apply_filters( 'generate_footer_widget_3_tablet_width', '50' ) ); ?> mobile-grid-100">
						<?php dynamic_sidebar('footer-3'); ?>
					</div>
					<?php endif;
					
					if ( $widgets >= 4 ) : ?>
					<div class="footer-widget-4 grid-parent grid-<?php echo absint( apply_filters( 'generate_footer_widget_4_width', $widget_width ) ); ?> tablet-grid-<?php echo absint( apply_filters( 'generate_footer_widget_4_tablet_width', '50' ) ); ?> mobile-grid-100">
						<?php dynamic_sidebar('footer-4'); ?>
					</div>
					<?php endif;
					
					if ( $widgets >= 5 ) : ?>
					<div class="footer-widget-5 grid-parent grid-<?php echo absint( apply_filters( 'generate_footer_widget_5_width', $widget_width ) ); ?> tablet-grid-<?php echo absint( apply_filters( 'generate_footer_widget_5_tablet_width', '50' ) ); ?> mobile-grid-100">
						<?php dynamic_sidebar('footer-5'); ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php
	endif;
	do_action( 'generate_after_footer_widgets' );
}
endif;

if ( ! function_exists( 'generate_construct_footer' ) ) :
/**
 * Build our footer
 * @since 1.3.42
 */
add_action( 'generate_footer','generate_construct_footer' );
function generate_construct_footer() {
	?>
	<footer class="site-info" itemtype="http://schema.org/WPFooter" itemscope="itemscope">
		<div class="inside-site-info <?php if ( 'full-width' !== generate_get_setting( 'footer_inner_width' ) ) : ?>grid-container grid-parent<?php endif; ?>">
			<p>
				European Services Strategy Unit, Duagh, Camp, Tralee, County Kerry, Ireland.<br>
				Copyright &copy; 1998-<?php echo date('Y'); ?> European Services Strategy Unit
			</p>
			<p><small>This website has been developed, maintained and hosted by <a href="https://webarchitects.coop">Webarchitects</a> since 1998.</small></p>
		</div>
	</footer><!-- .site-info -->
	<?php
}
endif;